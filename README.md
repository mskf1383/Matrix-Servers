# Matrix Servers

Find a Matrix server with your criterions!

- 📬: Email required
- 😎: Individual
- 🔗: Open web client

Provider | Homeserver | Public | Username & Password | SSO | Oauth | Location
-------- | ---------- | :----------------: | :----------------: | :-: | :---: | :------:
Conduit | https://conduit.koesters.xyz | ✔️ | ✔️ | ❌ | ❌ | 🇱🇺
Environments | https://matrix.envs.net [🔗](https://element.envs.net) | ✔️ | 📬 | ❌ | ❌ | 🇩🇪
Feneas | https://matrix.feneas.org [🔗](https://chat.feneas.org) | ❌ | ✔️ | ❌ | ❌ | 🇫🇮
Gnome | https://gnome.modular.im [🔗](https://gnome.element.io) | ❌ | 📬 | ❌ | ❌ | 🇩🇪
Halogen City | https://halogen.city [🔗](https://halogen.chat) | ✔️ | 📬 | ❌ | ❌ | 🇷🇴
KDE | https://kde.modular.im [🔗](https://webchat.kde.org) | ✔️ | 📬 | ❌ | ❌ | 🇩🇪
Librechurch | https://synod.im [🔗](https://web.synod.im) | ✔️ | 📬 | ❌ | ❌ | 🇩🇪
Matrix | https://matrix-client.matrix.org [🔗](https://app.element.io) | ✔️ | 📬 | ❌ | ✔️ | 🇺🇸
Mozilla | https://mozilla.modular.im [🔗](https://chat.mozilla.org) | ✔️ | ❌ | ✔️ | ✔️ | 🇺🇸
Nitrokey | https://nitro.chat | ✔️ | ✔️ | ❌ | ❌ | 🇺🇸
NLtrix | https://nltrix.net [🔗](https://element.nltrix.net/) | ✔️ | 📬 | ❌ | ❌ | 🇳🇱
PrivacyTools | https://chat.privacytools.io [🔗](https://element.privacytools.io) | ❌ | ✔️ | ❌ | ❌ | 🇺🇸
Studenten Net Twente | https://matrix.utwente.io [🔗](https://chat.utwente.io) | ✔️ | 📬 | ❌ | ❌ | 🇳🇱
Stux | https://stux.chat [🔗](https://stux.chat) | ✔️ | ✔️ | ❌ | ❌ | 🇳🇱
tchncs 😎 | https://matrix.tchncs.de/ [🔗](https://chat.tchncs.de) | ✔️ | 📬 | ❌ | ❌ | 🇩🇪
WIIZ | https://wiiz.ir [🔗](https://chat.wiiz.ir) | ✔️ | ✔️ | ❌ | ❌ | 🇮🇷
